# O teste #

O teste consiste em duas telas, sendo listagem e cadastro de álbuns de música, devendo ser entregue como estiver no horário estipulado no email.
Cada implementação significativa deve receber um commit (Ex: Header, Menu, Listagem, Upload, etc).
Não é preciso terminar todas as funcionalidades da lista pois o objetivo do teste é analisar a organização do desenvolvedor e forma de trabalho.
A criação do layout é livre, podendo utilizar algum framewok, como Bootstrap ou UI-Kit ou template.


### Página de Listagem de Álbuns ###

* Header com logo
* Tabela com álbuns (Capa, Artista/Banda, Título, Nº de músicas, Ação de excluir)
* Carregamento em ajax
* Páginação

### Página de Cadastro ###

* Formulário de cadastro com:
* Upload/previsualização de imagem de capa;
* Nome do artista;
* Título do Álbum;
* Upload múltiplo de músicas com:
* Player para ouvir música antes de confirmar upload
* Obter título da música automaticamente (via ID3 Tag)
* Possibilidade de remover música da lista de upload;
* Loader simulando upload e redirecionamento para a página de listagem ao salvar;
