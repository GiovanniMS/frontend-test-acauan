<?

include ('inc-header.php');

?>

<section>
	<div class="container">
		<div class="col-md-12">
			<div class="title">
				<h2>Cadastro de álbuns</h2>
			</div>

			<form action="#" method="post" enctype="multipart/form-data" class="js-form-album col-md-8" >
			  <div class="form-group col-md-12">
			    <label for="banda-title">Artista/banda:</label>
			    <input type="text" class="form-control" id="banda" name="banda_title">
			  </div>
			  <div class="form-group col-md-12">
			     <label for="album-title">Título do álbum:</label>
			    <input type="text" class="form-control" id="album-title" name="album_title">
			  </div>
			  
			  <div class="form-group col-md-12 dropzone" id="dropzone">				
			    
			  </div>
			  
			  <div class="form-group col-md-12">
			  	<button type="submit" class="btn btn-default">Salvar Album</button>
			  </div>
			</form>
		
			
		</div>
	</div>

</section>

<?

include ('inc-footer.php');

?>