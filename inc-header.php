
<? 
include ('functions/functions.php');
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="/frontend-test-acauan/" >

	<meta charset="UTF-8">
	<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">

	<title>Teste Start</title>
	
	<link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">

	<link rel="stylesheet" href="assets/css/vendor/dropzone.css">
	<link rel="stylesheet" href="assets/css/vendor/basic.css">

	

	<link rel="stylesheet" href="assets/css/style.css">

	


</head>
<body>

<header>
	
<div class="container" >
	<div class="row ">
		<div class="container">

			<div class="col-md-10 offset-md-1">
				
					<figure >
						<img src="assets/img/logo.jpg" alt="Logo">
					</figure>
				

				

				
			</div>
		</div>
		
	</div>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  
		  	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item ">
		        <a class="nav-link" href="home">Home</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="cadastro">Cadastro</a>
		      </li>
		  	</ul>
		  </div>
		
	</nav>



</div>

</header>